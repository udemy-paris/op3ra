<?php

namespace App\Tests;

class ComposerTest extends AbstractApiTest
{
    private static $testComposer = [
        'firstName' => 'Wolfgang',
        'lastName' => 'Mozart',
        'dateOfBirth' => '1756-01-27',
        'countryCode' => 'AT',
    ];

    /**
     * @depends testCreate
     */
    public function testIndex(): void
    {
        $response = $this->get('/composer', static::$testUserToken);
        $this->assertSame(200, $response->getStatusCode());
        $this->assertJson($response->getContent());

        $json = json_decode($response->getContent(), true);
        $this->assertTrue(in_array(static::$testComposer, $json));
    }

    public function testCreate(): void
    {
        $invalidComposer = static::$testComposer;
        unset($invalidComposer['lastName']);
        $response = $this->post('/composer', $invalidComposer, static::$testAdminToken);
        $this->assertSame(422, $response->getStatusCode());

        $response = $this->post('/composer', static::$testComposer, static::$testAdminToken);
        $this->assertSame(201, $response->getStatusCode());
        $this->assertJson($response->getContent());

        $json = json_decode($response->getContent(), true);
        $this->assertNotEmpty($json['id']);
        static::$testComposer['id'] = $json['id'];
    }

    /**
     * @depends testCreate
     */
    public function testShow(): void
    {
        $response = $this->get('/composer/' . static::$testComposer['id'], static::$testUserToken);
        $this->assertSame(200, $response->getStatusCode());
        $this->assertJson($response->getContent());

        $json = json_decode($response->getContent(), true);
        $this->assertEquals(static::$testComposer, $json);
    }

    /**
     * @depends testCreate
     */
    public function testUpdate(): void
    {
        static::$testComposer['firstName'] = 'Wolfgang Amadeus';
        $response = $this->put('/composer/' . static::$testComposer['id'], static::$testComposer, static::$testAdminToken);
        $this->assertSame(200, $response->getStatusCode());
        $this->assertJson($response->getContent());

        $json = json_decode($response->getContent(), true);
        $this->assertEquals(static::$testComposer, $json);
    }

    /**
     * @depends testCreate
     */
    public function testDelete(): void
    {
        $response = $this->delete('/composer/' . static::$testComposer['id'], static::$testAdminToken);
        $this->assertSame(204, $response->getStatusCode());
    }
}
