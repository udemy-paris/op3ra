<?php

namespace App\Controller;

use App\Entity\Symphony;
use App\Repository\SymphonyRepository;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[OA\Tag(name: 'symphony')]
#[IsGranted('ROLE_USER')]
class SymphonyController extends AbstractController
{
    #[OA\Response(
        response: 200,
        description: 'Successful response',
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(ref: new Model(type: Symphony::class))
        )
    )]
    #[Route('/symphony', name: 'app_symphony_index', methods:['GET'])]
    public function index(SymphonyRepository $repo): JsonResponse
    {
        return $this->json($repo->findAll());
    }

    #[OA\Response(
        response: 200,
        description: 'Successful response',
        content: new Model(type: Symphony::class)
    )]
    #[Route('/symphony/{id}', name: 'app_symphony_show', methods:['GET'])]
    public function show(Symphony $symphony): JsonResponse
    {
        return $this->json($symphony);
    }

    #[OA\RequestBody(
        content: new OA\JsonContent(
            type: 'object',
            ref: new Model(type: Symphony::class, groups: ['create']),
        )
    )]
    #[OA\Response(
        response: 200,
        description: 'Successful response',
        content: new Model(type: Symphony::class)
    )]
    #[IsGranted('ROLE_ADMIN')]
    #[Route('/symphony', name: 'app_symphony_create', methods:['POST'])]
    public function create(SymphonyRepository $repo, SerializerInterface $serializer, ValidatorInterface $validator, Request $request): JsonResponse
    {
        $symphony = $serializer->deserialize($request->getContent(), Symphony::class, 'json', [
            'groups' => ['create'],
        ]);
        $errors = $validator->validate($symphony);
        if (count($errors) > 0) {
            return $this->json($errors, 422);
        }
        $repo->save($symphony, true);
        return $this->json($symphony, 201);
    }

    #[OA\RequestBody(
        content: new OA\JsonContent(
            type: 'object',
            ref: new Model(type: Symphony::class, groups: ['update']),
        )
    )]
    #[OA\Response(
        response: 200,
        description: 'Successful response',
        content: new Model(type: Symphony::class)
    )]
    #[IsGranted('ROLE_ADMIN')]
    #[Route('/symphony/{id}', name: 'app_symphony_update', methods:['PUT'])]
    public function update(SymphonyRepository $repo, SerializerInterface $serializer, ValidatorInterface $validator, Symphony $symphony, Request $request): JsonResponse
    {
        $symphony = $serializer->deserialize($request->getContent(), Symphony::class, 'json', [
            AbstractNormalizer::OBJECT_TO_POPULATE => $symphony,
            'groups' => ['update'],
        ]);
        $errors = $validator->validate($symphony);
        if (count($errors) > 0) {
            return $this->json($errors, 422);
        }
        $repo->save($symphony, true);
        return $this->json($symphony);
    }

    #[OA\Response(
        response: 204,
        description: 'Successful response'
    )]
    #[IsGranted('ROLE_ADMIN')]
    #[Route('/symphony/{id}', name: 'app_symphony_delete', methods:['DELETE'])]
    public function delete(SymphonyRepository $repo, Symphony $symphony): JsonResponse
    {
        $repo->remove($symphony, true);
        return $this->json('', 204);
    }
}
